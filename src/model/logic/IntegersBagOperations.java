package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {
//Agregar aqu� operaciones nuevas
	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	public int sum(IntegersBag bag){
	    int sum=0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				sum += iter.next();
			}
			
		}
		return sum;
	}
	public double computeMedian(IntegersBag bag){
		int median = Integer.MIN_VALUE;
		int constant = 1;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			IntegersBag bag2 = bag;
			Iterator<Integer> iter2 = bag2.getIterator();
			while(iter.hasNext())
			{
				iter.next();
				constant++;
				if(constant==2)
				{
					constant=0;
					median=iter2.next();
				}
				
			}

		}
		return median;
	}
	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value){
					min = value;
				}
			}
			
		}
		return min;
	}
	
	
}
